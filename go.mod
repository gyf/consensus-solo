module chainmaker.org/chainmaker/consensus-solo/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.0
	chainmaker.org/chainmaker/consensus-utils/v2 v2.2.0
	chainmaker.org/chainmaker/logger/v2 v2.2.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.0
	chainmaker.org/chainmaker/protocol/v2 v2.2.0
	chainmaker.org/chainmaker/utils/v2 v2.2.0
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
)
